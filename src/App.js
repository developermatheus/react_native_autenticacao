import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, Card, CardSection, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {

    state = { logado: null };

    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyAXjMQjYuvEnkqSmRZVShGd8KwUlOXXAkk',
            authDomain: 'autenticacao-42dfc.firebaseapp.com',
            databaseURL: "https://autenticacao-42dfc.firebaseio.com",
            projectId: 'autenticacao-42dfc',
            storageBucket: 'autenticacao-42dfc.appspot.com',
            messagingSenderId: '242379789677'
        })

        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                this.setState({ logado: true });
            } else {
                this.setState({ logado: false });
            }
        });
    }

    renderizaConteudo() {

        switch(this.state.logado) {
            case true:
                return (
                    <Card>
                        <CardSection>
                            <Button botaoPressionado={() => firebase.auth().signOut()}>Sair</Button>
                        </CardSection>
                    </Card>
                );

            case false:
                return <LoginForm />

            default:
                return (
                    <Card>
                        <CardSection>
                            <Spinner size="large" />
                        </CardSection>
                    </Card>
                );
        }
    }

    render() {
        return (
            <View>
                <Header tituloHeader="Autenticação" />
                {/* <LoginForm /> */}
                {this.renderizaConteudo()}
            </View>

        );
    }
}

export default App;
