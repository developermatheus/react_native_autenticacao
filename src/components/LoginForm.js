import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
import { Button, Card, CardSection, Input, Spinner } from './common';

class LoginForm extends Component {

    state = { email: '', senha: '', erro: '', carregando: false };
    
    autenticacao () {
        
        const { email, senha } = this.state;
        this.setState({ error: '', carregando: true })

        firebase.auth().signInWithEmailAndPassword(email, senha)
            .then(this.loginRealizado.bind(this))
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, senha)
                    .then(this.loginRealizado.bind(this))
                    .catch(this.loginNaoRealizado.bind(this));
            });
    }
        
    loginRealizado() {
        this.setState({ email: '', senha: '', carregando: false });
    }

    loginNaoRealizado() {
        this.setState({ erro: 'Erro de autenticação', carregando: false });
    }

    mostraComponente() {
        if(this.state.carregando) {
            return <Spinner />;
        } else {
            return (
                <Button botaoPressionado={this.autenticacao.bind(this)}>
                    Login
                </Button>
            )
        }

    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input label='Email' placeholder='Digite seu email' corDePlaceholder="#ccc" value={this.state.email} onChangeText={email => this.setState({ email })} />
                </CardSection>

                <CardSection>
                    <Input escondeCaracteres label='Senha' placeholder='Digite sua senha' corDePlaceholder="#ccc" value={this.state.senha} onChangeText={senha => this.setState({ senha })} />
                </CardSection>

                <Text style={styles.errorTextStyle}>
                    {this.state.erro}
                </Text>

                <CardSection>
                    {this.mostraComponente()}
                </CardSection>
            </Card>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
}

// TextInputs tem por padrão 0 altura e largura, precisamos tratar isso com estilos

export default LoginForm;