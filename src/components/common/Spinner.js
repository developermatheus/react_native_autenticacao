import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ tamanho }) => {

    const { estiloSpinner } = edicoes;

    return (
        <View style={estiloSpinner}>
            <ActivityIndicator size={tamanho || 'large'} />
        </View>
    );

}

const edicoes = {
    estiloSpinner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}

export { Spinner };
