// Importar bibliotecas para fazer o componente
import React from 'react';
import { Text, View } from 'react-native';

// Criar o componente
const Header = (props) => {
    const { textoHeader, estiloView } = estilos; // Referência do estilo

    return (
        <View style={estiloView}>
            <Text style={textoHeader}>{props.tituloHeader}</Text>
        </View>
    );
};

const estilos = {

    estiloView: {
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        // paddingTop: 15
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2, //for iOS
        elevation: 10, //for Android
        position: 'relative'
    },

    textoHeader: {
        fontSize: 20,
    }
};

// Fazer o componente visível para as outras partes do app
export { Header };
